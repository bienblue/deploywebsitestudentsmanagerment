# CloudComputing
Thông tin nhóm 03:
    Tên đề tài: Viết ứng dụng kết hợp nhiều docker với nhau
    Thành Viên:

        Hồ Ngọc Biển       - 19133011
        Nguyễn Minh Khoa   - 19133029
        Nguyễn Phước Tuân  - 19133060
# Các bước cài đặt
    Bước 1: Tạo ba EC2(hoặc bất kỳ server nào) để chạy 3 container tương ứng với UI, Backend, Database
    Bước 2: Thay đổi địa chỉ ở các file sau
        backend/src/main/resources/application.properties/ 
            -spring.datasource.url = "Địa chỉ mysql"
        backend/src/main/java/com/cloudcomputing/backend/api/StudentAPI.java/
            # Địa chỉ WebUI
            -@CrossOrigin(origins = "http://ec2-3-213-133-65.compute-1.amazonaws.com:5000/")
        StudentManagement/StudentManagement/Controllers/StudentController.cs/
            # Địa chỉ Backend
            - private string url = "http://ec2-3-212-26-249.compute-1.amazonaws.com:8080";
        StudentManagement/StudentManagement/Views/Student/AddStudent.cshtml/
            # Địa chỉ Backend
            url: 'http://ec2-3-212-26-249.compute-1.amazonaws.com:8080/student/add',




